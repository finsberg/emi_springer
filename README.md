# SpringerBriefs book on EMI model

This repository contains the source code and instructions for how to add your chapter to the EMI SpringerBriefs book.

## Guidelines for authors

All authors should read section 2.3 - 2.9 in the [instruction manual](template/guideline/authinst.pdf) for details on how to format their chapter.
You can also have a look at the [reference guide](template/guideline/refguide.pdf) which contains more information about the available environments.

### Example chapters
You will also find three example chapters in this repository

* A [minimal chapter](chapters/minimal/main.tex) that contains the minimum amount of information you need in order to make it compile
    - You can use this as a skeleton when copy pasting your own chapter

* A [template chapter](chapters/template/main.tex) that shows usage of the different environments that are available in the Springer class.
    - You can use this as a guide to write more fancy latex environments such as text boxes, code blocks etc.
    - You can also have a look at the [pre-compiled version](template/editor/editorsample.pdf) of this chapter

* A [real chapter](chapters/Derivation_of_EMI/main.tex) written by Aslak and Karoline
    - You can use this as a comparison with your own chapter

### Labels and references
We want to keep labels consistent. The table below show the conventions used for the different types of labels. Note that in order to avoid conflicting labels you should add your initials in the beginning of the label name (after the colon). Below you can see examples with the initials HF (Henrik Finsberg).

|Type     | Label starts with  | Example label (hf)              | 
|--------:|-------------------:|--------------------------------:|
|Figure   | `fig`              | `\label{fig:hf_cell}`           |
|Table    | `tab`              | `\label{tab:hf_parameters}`     |
|Section  | `sec`              | `\label{sec:hf_model}`          |
|Equation | `eq`               | `\label{eq:hf_model}`           |
|Theorem  | `thm`              | `\label{thm:hf_model}`          |

Use `\eqref` to reference equations, and use `\ref` otherwise.

### Bibliography

All authors should provide a `.bib`-file which contains all references in BibTex format. 
To find the BibTex entry, you can search on Google scholar (or any other search engine for papers), select BibTex and copy all the fields in the `.bib`-file.

If you need to cite a chapter in this book then add the following entry to your `.bib`-file:

```
@incollection{emibookchapter,
  author       = {Authors of the chapters you want to cite}, 
  title        = {The title of the chapter you want to cite},
  booktitle    = {EMI: Cell-based mathematical models of excitable tissue - models, methods and software},
  publisher    = {Springer},
  year         = 2020,
  editor       = {Tveito, Aslak and Rognes, Marie E and Mardal, Kent-Andre}
}
```


## Adding your chapter to the book

All authors are granted write access to the repo in order to make things a bit simpler. The first thing you should do in order to add you chapter to the book is to clone this repository and create a new branch
```
git clone git@bitbucket.org:finsberg/emi_springer.git
git checkout -b <username>/<chapter name>
```
Here `username` is your username or initials, and `chapter name` is some short, compact version of the name of the chapter that you want to add. 

In order to add your chapter to the book please do the following

1. Create a new sub-folder in the folder named `chapters`, and put all your files in this folder.
    - Make sure that your main latex document is called `main.tex`.
    - Do not use `\include` or `\input`. Instead you should have one single LaTex file (i.e `main.tex`).
    - Do not use spaces in the names of files or folder. Use an underscore (`_`) instead.

2. Remove all code in your latex document that is defined outside the document (including `\begin{document}` and `\end{document}`), such as packages and new commands.
    - Please consult the example chapters for details.
    - If you are using any fancy packages, then you can add these in the latex file called [`packages.tex`](packages.tex).
    - If you have defined your own commands then put these commands in the latex file called [`commands.tex`](commands.tex).
    - Add the path to your figures in a block inside `graphicspath` (see example chapters)
    - For you references use the full path to the `.bib`-file and use the style `spbasic` (`\bibliographystyle{spbasic}`)

3. Include your main latex file in the file called [`book.tex`](book.tex).
    - Add the line `\include{chapters/<name of chapter>/main}` between thee command `\mainmatter` and `\backmatter`.

4. Add your name, email and affiliation to the list of contributors
    - Edit the file [contriblist.tex](contriblist.tex)

### Verify successful compilation
You can check that your chapter is successfully compiled by typing

```
make
```
in the root directory of this repo. In order for this to work you need to also have some latex packages installed. 
It is also possible to compile the document within a docker container, by first building a docker image using the provided [`Dockerfile`](Dockerfile) or using the existing image `finsberg/latex_full:latest`. This is useful if you are using Windows or you don't have all the required LaTex packages already installed. 

If you execute command
```
docker run -ti --name emi_book -v $(pwd):/home/shared -w /home/shared finsberg/latex_full:latest
```
in the root directory of this repository, you will spin up a container with all necessary latex packages pre-installed. 

Alternatively, you can commit and push your changes to the repository (on your branch). This will trigger a build in the continuos integration system using bitbucket-pipelines. If the pipeline is successful you will see a green checkmark, and you can inspect the built version of the book by clicking at the tab called [Artifacts](https://confluence.atlassian.com/bitbucket/files/935389074/954837025/1/1533266568248/artifacttab.png) and download the compiled document. 

### Finished

If you have added the chapter, and verified that everything looks good, you should submit a pull request. Click on "Pull requests"->"Create pull request", and select your branch in the box to the left, and the master branch to the right.

## Having issues?

If you have issues please click on the issues tab and create a new issue.

## Other questions
If you have any questions, please send an email to <henriknf@simula.no>.